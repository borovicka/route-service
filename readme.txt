Build application:
-------------------
Build application by maven (in project folder):
    mvn clean install


Run application:
------------------
To run application go to project folder and run command:
    java -jar target/route-service-1.0-SNAPSHOT.jar

Rest endpoint is:
    http://localhost:8080/routing/{origin}/{destination}
