package cz.jj.example.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.jj.example.dto.Country;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DataProvider {

    private Map<String, Country> countries;

    @PostConstruct
    public void init() {
        countries = Arrays.stream(readJson()).collect(Collectors.toMap(Country::getCca3, country -> country));
    }

    private Country[] readJson() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(new URL("https://raw.githubusercontent.com/mledoze/countries/master/countries.json"), Country[].class);
        } catch (IOException e) {
            throw new RuntimeException("Cannot obtain data", e);
        }
    }

    public Map<String, Country> getCountries() {
        return countries;
    }

    public Country findCountry(String countryCode) {
        return countries.get(countryCode);
    }
}
