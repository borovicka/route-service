package cz.jj.example.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Country {

    private String cca3;
    private List<String> borders;

    public String getCca3() {
        return cca3;
    }

    public List<String> getBorders() {
        return borders;
    }
}
