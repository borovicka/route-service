package cz.jj.example.controller;

import cz.jj.example.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/routing")
public class RouteController {

    @Autowired
    private RouteService routeService;


    @GetMapping("/")
    public String index() {
        System.out.println("Hello");
        return "hello";
    }

    @GetMapping("/{origin}/{destination}")
    public ResponseEntity<List<String>> route(@PathVariable("origin") String from, @PathVariable("destination") String to) {

        Optional<List<String>> route = routeService.findRoute(from, to);

        if(route.isPresent()) {
            return new ResponseEntity<>(route.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
