package cz.jj.example.service;

import cz.jj.example.data.DataProvider;
import cz.jj.example.dto.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class RouteService {

    @Autowired
    private DataProvider dataProvider;

    public Optional<List<String>> findRoute(String from, String to) {
        List<String> route = new ArrayList<>();
        route.add(from);

        if(from.equals(to)) {
            return Optional.of(route);
        }

        List<List<String>> foundRoutes = new ArrayList<>();
        Map<String, Integer> visitedCountries = new HashMap<>();
        visitedCountries.put(from, 0);
        Country country = dataProvider.findCountry(from);

        searchInNeighbours(country.getBorders(), visitedCountries, to, route, foundRoutes, 1);

        if(foundRoutes.isEmpty()) {
            return Optional.empty();
        }

        if(foundRoutes.size() > 1) {
            foundRoutes.sort(Comparator.comparingInt(List::size));
        }

        return Optional.of(foundRoutes.get(0));

    }

    private void searchInNeighbours(List<String> collectionToSearch, Map<String, Integer> visitedCountries, String to, List<String> route, List<List<String>> foundRoutes, int level) {

        for (String neighbour : collectionToSearch) {

            if(visitedCountries.containsKey(neighbour) && visitedCountries.get(neighbour) <= level) {
                continue;
            }

            List<String> blindRoute = new ArrayList<>(route);
            blindRoute.add(neighbour);

            if(to.equals(neighbour)) {

                foundRoutes.add(blindRoute);

            } else {

                visitedCountries.put(neighbour, level);
                searchInNeighbours(dataProvider.findCountry(neighbour).getBorders(), visitedCountries, to, blindRoute, foundRoutes, level + 1);

            }
        }
    }
}
