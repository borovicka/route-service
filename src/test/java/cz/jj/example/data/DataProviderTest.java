package cz.jj.example.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.jj.example.dto.Country;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertEquals;


public class DataProviderTest {

    @Test
    public void testCountryIntegrity() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {

            Country[] countryArray = objectMapper.readValue(new File("src/test/resources/testData.json"), Country[].class);
            assertEquals("ABW", countryArray[0].getCca3());

        } catch (IOException e) {
            throw new RuntimeException("Cannot obtain data", e);
        }
    }

    @Test
    public void testConnection() {
        ObjectMapper objectMapper = new ObjectMapper();

        try {

            Country[] countryArray = objectMapper.readValue(new URL("https://raw.githubusercontent.com/mledoze/countries/master/countries.json"), Country[].class);
            assertEquals("ABW", countryArray[0].getCca3());

        } catch (IOException e) {
            throw new RuntimeException("Cannot obtain data", e);
        }
    }

}


